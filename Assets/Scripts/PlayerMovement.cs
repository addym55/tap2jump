﻿using System.Runtime.InteropServices;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D rg;
    private BoxCollider2D boxCollider;

    public LayerMask platformLayerMask;
    public float runSpeed = 40f,
                jumpForce = 10f,
                fallMultiplier = 2.5f;

    float horizontalMove = 0f;
    bool jump = false;
    bool crouch = false;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        LoadReferences();
    }

    private void LoadReferences()
    {
        rg = GetComponent<Rigidbody2D>();
        boxCollider = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;

        if (rg.velocity.y < 0)
        {
            rg.velocity += Vector2.up * Physics2D.gravity.y * fallMultiplier * Time.deltaTime;
        }
        else if (rg.velocity.y > 0 && !Input.GetButtonDown("Jump"))
        {
            rg.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
        }


        if (Input.GetButtonDown("Jump") && isGrounded())
        {
            Debug.Log("Jump");
            rg.velocity = Vector2.up * jumpForce;

        }
    }

    void FixedUpdate()
    {
        Move(horizontalMove, jump);
    }

    void Move(float hMove, bool jump)
    {
        rg.velocity = new Vector2(horizontalMove * runSpeed * Time.fixedDeltaTime, rg.velocity.y);

    }

    private bool isGrounded()
    {
        float extraHeight = 1f;
        RaycastHit2D raycastHit = Physics2D.BoxCast(boxCollider.bounds.center, boxCollider.bounds.size, 0f, Vector2.down, extraHeight, platformLayerMask);
        return raycastHit.collider != null;
    }
}

