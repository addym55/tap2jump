﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    internal static UnityAction onGameOver;

    private Transform canvasRoot,
                    gameOverPanel;
    // private Text txtGameOver;
    private Button btnRetry;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        LoadReferences();
    }

    private void LoadReferences()
    {
        canvasRoot = transform.Find("Canvas");
        gameOverPanel = canvasRoot.Find("GameOverPanel");
        btnRetry = gameOverPanel.Find("BtnRetry").GetComponent<Button>();
        // txtGameOver = canvasRoot.Find("txtGameOver").GetComponent<Text>();
    }

    /// <summary>
    /// This function is called when the object becomes enabled and active.
    /// </summary>
    void OnEnable()
    {
        onGameOver += OnGameOver;
        btnRetry.onClick.AddListener(OnClickRetry);
    }
    /// <summary>
    /// This function is called when the behaviour becomes disabled or inactive.
    /// </summary>
    void OnDisable()
    {
        onGameOver -= OnGameOver;
        btnRetry.onClick.RemoveListener(OnClickRetry);
    }

    void OnGameOver()
    {
        gameOverPanel.gameObject.SetActive(true);
    }

    void OnClickRetry()
    {
        SceneManager.LoadScene("2");
    }
}
