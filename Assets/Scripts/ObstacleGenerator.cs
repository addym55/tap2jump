﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleGenerator : MonoBehaviour
{
    public GameObject[] obstacles;
    public Transform startPos, endPos, parent;
    public float moveSpeed = 10f;
    public float frequency = 0.5f;

    public List<GameObject> pool = new List<GameObject>();
    private bool isGameOver = false;
    private GameController gameController;

    void Awake()
    {
        LoadRefernces();
    }

    void LoadRefernces()
    {
        gameController = GetComponent<GameController>();

        Generate();
    }

    void OnEnable()
    {
        GameController.onGameOver += OnGameOver;
    }

    void OnDisable()
    {
        GameController.onGameOver -= OnGameOver;
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(ManageObstacles());
    }

    // Update is called once per frame
    void Update()
    {

    }

    void Generate()
    {
        for (int i = 0; i < obstacles.Length; i++)
        {
            int maxCopy = Random.Range(2, 4);
            for (int j = 0; j < maxCopy; j++)
            {
                GameObject go = Instantiate(obstacles[i], Vector3.zero, Quaternion.identity, parent) as GameObject;
                go.transform.position = startPos.position;
                go.SetActive(false);
                pool.Add(go);
            }
        }
    }

    IEnumerator ManageObstacles()
    {
        while (!isGameOver)
        {
            int index = Random.Range(0, pool.Count);
            StartCoroutine(MoveObstacle(pool[index]));
            yield return new WaitForSeconds(frequency);
        }
        yield break;
    }

    IEnumerator MoveObstacle(GameObject go)
    {
        if (go.transform.position.x < startPos.position.x && go.transform.position.x > endPos.position.x)
            yield break;
        go.transform.position = startPos.position;
        go.SetActive(true);
        while (go.transform.position.x > endPos.position.x)
        {
            go.transform.Translate(Vector2.left * moveSpeed * Time.deltaTime);
            yield return null;
        }
        go.SetActive(false);
    }

    private void OnGameOver()
    {
        isGameOver = true;
    }
}
