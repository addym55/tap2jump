﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float jumpForce = 1;
    public LayerMask platformLayerMask;
    private float playerPos;
    private BoxCollider2D boxCollider;
    private Rigidbody2D rg;


    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        LoadReferences();
    }

    private void LoadReferences()
    {
        rg = GetComponent<Rigidbody2D>();
        boxCollider = GetComponent<BoxCollider2D>();
        playerPos = transform.position.x;
    }

    void OnEnable()
    {
        GameController.onGameOver += OnGameOver;
    }


    void OnDisable()
    {
        GameController.onGameOver -= OnGameOver;
    }

    private void OnGameOver()
    {
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x != playerPos)
        {
            if (GameController.onGameOver != null) GameController.onGameOver.Invoke();
        }

        if (Input.GetKeyDown(KeyCode.Space) && isGrounded())
        {
            Jump();
        }
    }

    /// <summary>
    /// This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    /// </summary>
    void FixedUpdate()
    {
        // Jump();
    }

    void Jump()
    {
        rg.AddForce(Vector2.up * jumpForce * rg.mass * rg.gravityScale * 20f);
    }

    private bool isGrounded()
    {
        float extraHeight = .1f;
        RaycastHit2D raycastHit = Physics2D.BoxCast(boxCollider.bounds.center, boxCollider.bounds.size, 0f, Vector2.down, extraHeight, platformLayerMask);
        return raycastHit.collider != null;
    }
}
